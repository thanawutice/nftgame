/** Connect to Moralis server */
const serverUrl = "https://sykr9o7s6eev.usemoralis.com:2053/server";
const appId = "WoxPomvhlpRRGiG1BvkJikecoUT0fIWaNFiSBNcY";
Moralis.start({ serverUrl, appId });
const CONTRACT_ADDRESS = "0x5e9De8Db2B946A2dCE29181154A9aDA3E7C700E7";

/** Add from here down */
async function init() {
    try {
        let user = Moralis.User.current();
        if (!user) {
            console.log('No Login')
            $("#btn-logout").hide();
            $("#btn-login").click( async () =>{
                try {
                    user = await Moralis.Web3.authenticate();
                    console.log('USER LOGIN',user)
                    window.location.reload();
                }catch(error){
                    console.log('USER LOGIN ERROR',error)
                }
            })
        }else{
            console.log('login success !!');
            $("#btn-logout").show();
            $("#btn-login").hide();
            renderGame();
        }
        
    } catch(error) {
        console.log(error)
    }
}

async function renderGame(){
    $("#pet_row").html("");

    //Get and Render Properties from SC
    window.web3 = await Moralis.Web3.enable();
    let abi = await getAbi();
    
    let contract = new web3.eth.Contract(abi, CONTRACT_ADDRESS);
    
    try {
        let array = await contract.methods.getAllTokensForUser(ethereum.selectedAddress).call({from: ethereum.selectedAddress});
        console.log("asdasdasdasd",array)
        if(array.length == 0) return;
        array.forEach(async (petId) =>{
            let details = await contract.methods.getTokenDetails(petId).call({from: ethereum.selectedAddress});

            renderPet(petId,details);

        })

        let shops = await contract.methods.getAllTokensForUser(ethereum.selectedAddress).call({from: ethereum.selectedAddress});
        console.log("asdasdasdasd",shops)
        if(shops.length == 0) return;
        shops.forEach(async (petId) =>{
            let details = await contract.methods.getTokenDetails(petId).call({from: ethereum.selectedAddress});

            renderPet(petId,details);

        })

    } catch (error) {
        console.log(error.message);
    }

    // let data = await contract.methods.getTokenDetails(petId).call({from: ethereum.selectedAddress});
    
    $("#game").show();
}

function renderShop(){
    let htmlString = `
    <div class="col-md-4 card mx-1" id="pet_${id}">
        <img class="card-img-top pet_img" src="pet.png">
        <div class="card-body">
            <div>ID: <span class="pet_id">${id}</span></div>
            <div>Damage: <span class="pet_damage">${data.damage}</span></div>
            <div>Magic: <span class="pet_magic">${data.magic}</span></div>
            <div>Endrurance: <span class="pet_endrurance">${data.endrurance}</span></div>
            <div>Time to starvation: <span class="pet_starvation_time">${deathTime}</span></div>
            <div class="progress">
                <div class="progress-bar" style="width:${percentageString}">

                </div>
            </div>
            <div data-pet-id="${id}" class="feed_button btn btn-primary btn-block">Feed</div>
        </div>
    </div>`;

    let element = $.parseHTML(htmlString);
    $("#pet_row").append(element)
}

function renderPet(id,data){
    let now = new Date();
    let maxTime = data.endrurance;
    let currentUnix = Math.floor(now.getTime() / 1000);
    let secondsLeft = (parseInt(data.lastMeal) + parseInt(data.endrurance)) - currentUnix;
    let percentageLeft = secondsLeft / maxTime;
    let percentageString = (percentageLeft * 100) + '%'

    let deathTime = new Date((parseInt(data.lastMeal) + parseInt(data.endrurance)) * 1000);
    
    if(now > deathTime){
        
    }

    let interval = setInterval( () => {
        console.log("interval")
        let now = new Date();
        let maxTime = data.endrurance;
        let currentUnix = Math.floor(now.getTime() / 1000);
        let secondsLeft = (parseInt(data.lastMeal) + parseInt(data.endrurance)) - currentUnix;
        let percentageLeft = secondsLeft / maxTime;
        let percentageString = (percentageLeft * 100) + '%'
        $(`#pet_${id} .progress-bar`).css("width",percentageString);
        if(percentageLeft < 0){
            clearInterval(interval);
        }
    },5000)

    let htmlString = `
    <div class="col-md-4 card mx-1" id="pet_${id}">
        <img class="card-img-top pet_img" src="pet.png">
        <div class="card-body">
            <div>ID: <span class="pet_id">${id}</span></div>
            <div>Damage: <span class="pet_damage">${data.damage}</span></div>
            <div>Magic: <span class="pet_magic">${data.magic}</span></div>
            <div>Endrurance: <span class="pet_endrurance">${data.endrurance}</span></div>
            <div>Time to starvation: <span class="pet_starvation_time">${deathTime}</span></div>
            <div class="progress">
                <div class="progress-bar" style="width:${percentageString}">

                </div>
            </div>
            <div data-pet-id="${id}" class="feed_button btn btn-primary btn-block">Feed</div>
        </div>
    </div>`;

    let element = $.parseHTML(htmlString);
    $("#pet_row").append(element)

    $(`#pet_${id} .feed_button`).click( () =>{
        feed(id);
    });

}

function getAbi(){
    return new Promise( (res) => {
        $.getJSON("Token.json",'utf8',( (json) =>{
            res(json.abi);
        }))
    })
}

async function feed(petId){
    console.log("petID",petId);
    let abi = await getAbi();
    let contract = new web3.eth.Contract(abi, CONTRACT_ADDRESS);
    contract.methods.feed(petId).send({from: ethereum.selectedAddress}).on("receipt",( () => {
        console.log("Feed Done!!");
        renderGame();
    }));
}

init();

async function logOut() {
  await Moralis.User.logOut();
  console.log("logged out");
  window.location.reload();
}

// document.getElementById("btn-login").onclick = login;
document.getElementById("btn-logout").onclick = logOut;